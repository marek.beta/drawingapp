package com.example.demo.model.enitity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DrawingSession {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long drawingSessionId;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PlayerRelation> playerRelations;

    private LocalDateTime date;
}


