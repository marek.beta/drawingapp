package com.example.demo.model.enitity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Data
public class PlayerRelation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userRelationId;
    @ManyToOne
    private Player player1;
    @ManyToOne
    private Player player2;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private DrawingSession drawingSession;

    public PlayerRelation(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
    }
}
