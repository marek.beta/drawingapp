package com.example.demo.service;

import com.example.demo.exception.NotEnoughPlayersException;
import com.example.demo.model.enitity.DrawingSession;
import com.example.demo.model.enitity.Player;
import com.example.demo.model.enitity.PlayerRelation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DrawService {

    private final DbService dbService;

    public void draw() throws Exception {
        List<Player> all = dbService.findAllPlayers();
        DrawingSession drawingSession = new DrawingSession();
        List<PlayerRelation> playerRelations = new ArrayList<>();
        List<Long> ids = new ArrayList<>();

        if (all.size() < 2) {
            throw new NotEnoughPlayersException();
        } else {
            for (Player player : all) {
                ids.add(player.getId());
                Player player2 = all.stream().filter(userEntity -> !ids.contains(userEntity.getId())).findAny()
                        .orElse(all.stream().filter(userEntity -> userEntity.getId() != player.getId()).findAny()
                                .orElseThrow(Exception::new));
                ids.add(player2.getId());
                PlayerRelation e = new PlayerRelation(player, player2);
                ids.remove(player.getId());
                playerRelations.add(e);
            }
        }
        drawingSession.setPlayerRelations(playerRelations);
        drawingSession.setDate(LocalDateTime.now());
        dbService.saveDrawingSession(drawingSession);
    }
    public Page<Map.Entry<LocalDateTime, Map<Player, Player>>> fetchDrawingSessionsByDate(String date, Pageable pageable){
        List<DrawingSession> drawingSessions = dbService.fetchDrawingSessionsByDate(LocalDateTime.parse(date));
        Map<LocalDateTime, Map<Player, Player>> map = drawingSessions.stream()
                .collect(Collectors.toMap(DrawingSession::getDate, this::mapSessionToResponse));

        return new PageImpl<>(new ArrayList<>(map.entrySet()),pageable, map.entrySet().size());
    }

    public Map<Player, Player> mapSessionToResponse(DrawingSession session){
        return session.getPlayerRelations().stream().collect(Collectors.toMap(PlayerRelation::getPlayer1, PlayerRelation::getPlayer2));
    }

}

