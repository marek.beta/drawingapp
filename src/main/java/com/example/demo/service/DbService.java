package com.example.demo.service;

import com.example.demo.model.enitity.DrawingSession;
import com.example.demo.model.enitity.Player;
import com.example.demo.repository.DrawingSessionRepository;
import com.example.demo.repository.PlayerRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class DbService {

    PlayerRepository playerRepository;

    DrawingSessionRepository drawingSessionRepository;

    public void savePlayer(Player player){
        playerRepository.saveAndFlush(player);
    }

    public Optional<Player> fetchPlayerByEmail(String email) {
       return playerRepository.findByEmail(email);
    }

    public void deletePlayerByEmail(String email){
        playerRepository.deleteByEmail(email);
    }

     public List<Player> findAllPlayers(){
        return playerRepository.findAll();
    }

    void saveDrawingSession(DrawingSession drawingSession){
         drawingSessionRepository.saveAndFlush(drawingSession);
    }

    List<DrawingSession> fetchDrawingSessionsByDate(LocalDateTime date){
        return drawingSessionRepository.fetchSessionsByDate(date);
    }
}
