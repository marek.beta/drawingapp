package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

@SpringBootApplication
public class DrawingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DrawingApplication.class, args);
	}
}