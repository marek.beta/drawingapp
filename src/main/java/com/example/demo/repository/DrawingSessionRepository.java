package com.example.demo.repository;

import com.example.demo.model.enitity.DrawingSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface DrawingSessionRepository extends JpaRepository<DrawingSession, Long> {

    @Query("select d from DrawingSession d where d.date is not null and d.date >= :lastUpdateDate")
    List<DrawingSession> fetchSessionsByDate(LocalDateTime lastUpdateDate);
}
