package com.example.demo.controller;

import com.example.demo.model.enitity.Player;
import com.example.demo.service.DrawService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Map;

@RestController
@RequestMapping("api/v1/draw")
@RequiredArgsConstructor
public class DrawController {

    private final DrawService drawService;

    @Operation(description = "method to initialize drawing session")
    @PostMapping
    public ResponseEntity initializeDrawing() throws Exception {
        drawService.draw();
        return new ResponseEntity(HttpStatus.OK);
    }

    @Operation(description = "method to fetch all drawing sessions that occurred from a provided date in format - yyyy-mm-ddThh:mm:ss")
    @GetMapping()
    public ResponseEntity<Page<Map.Entry<LocalDateTime, Map<Player, Player>>>> fetchDrawingSessionByDate(@RequestParam String date, Pageable pageable) {
        return new ResponseEntity<>(drawService.fetchDrawingSessionsByDate(date, pageable), HttpStatus.OK);
    }
}
