package com.example.demo.controller;

import com.example.demo.model.enitity.Player;
import com.example.demo.service.DbService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE,makeFinal = true)
public class PlayerController {
    DbService dbService;

    @Operation(description = "method to register a new user in database with a given email")
    @PostMapping(path = "/save")
    public ResponseEntity savePlayer(@RequestBody Player player){
        dbService.savePlayer(player);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @Operation(description = "method to fetch list of all users")
    @GetMapping
    public ResponseEntity<List<Player>> getPlayers(){
        return new ResponseEntity<>(dbService.findAllPlayers(),HttpStatus.OK);
    }
    @Operation(description = "method to fetch a user by its email")
    @GetMapping(path = "/{email}")
    public ResponseEntity<Player> getPlayerByEmail(@PathVariable String email){
        return new ResponseEntity<>(dbService.fetchPlayerByEmail(email).orElseThrow(EntityNotFoundException::new),HttpStatus.OK);
    }
    @Operation(description = "method to delete user by its email")
    @DeleteMapping(path = "/{email}")
    public ResponseEntity deletePlayerByEmail(@PathVariable String email){
        dbService.deletePlayerByEmail(email);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    @Operation(description = "put method to update specific user")
    @PutMapping(path = "/{email}")
    public ResponseEntity updatePutPlayer(@PathVariable String email, @RequestBody Player player){
        Player toBeUpdated = dbService.fetchPlayerByEmail(email).orElseThrow(EntityNotFoundException::new);
        toBeUpdated.setUsername(player.getUsername());
        toBeUpdated.setEmail(player.getEmail());
        dbService.savePlayer(toBeUpdated);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
